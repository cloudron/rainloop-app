[0.1.0]
* Initial version

[0.1.1]
* Update to upstream version 1.10.2.143

[0.1.2]
* Update base image
* Update to upstream version 1.10.3.151

[0.1.3]
* Update post install

[0.2.0]
* Update RainLoop to 1.10.4.183

[0.3.0]
* Update RainLoop to 1.10.5.192

[0.4.0]
* Update base image to 0.10.0

[0.5.0]
* Enable PGP
* Set max upload limit to 25MB

[0.6.0]
* Update RainLoop to 1.11.0.203

[0.7.0]
* Update RainLoop to 1.11.1

[0.8.0]
* Enable admin panel access

[0.8.1]
* Cleanup PHP sessions

[1.0.0]
* Fix permission issues that prevents start up

[1.1.0]
* Update RainLoop to 1.11.2

[1.1.1]
* Update RainLoop to 1.11.3

[1.2.0]
* Add documentationUrl
* Disable admin page by default. Can be enabled using the web terminal.

[1.3.0]
* Update POSTINSTALL
* Disable short login
* Remove mis-configured disabled domains

[1.4.0]
* Add Cloudron multi-domain support

[1.5.0]
* Update RainLoop to 1.12.0

[1.5.1]
* security: Make data directory inaccessible

[1.6.0]
* Make filters use the managesieve STARTTLS protocol

[1.6.1]
* Update Rainloop to 1.12.1

[1.6.2]
* Add support for [external domains](https://cloudron.io/documentation/apps/rainloop/#external-domains)

[1.7.0]
* Use latest base image

[1.8.0]
* Update manifest to v2
* Update Rainloop to 1.13.0

[1.9.0]
* Update Rainloop to 1.14.0

[1.10.0]
* Use latest base image 2.0.0

[1.11.0]
* Read in custom PHP configuration from `/app/data/php.ini`

[1.12.0]
* Update Rainloop to 1.15.0
* [Full changelog](https://github.com/RainLoop/rainloop-webmail/releases/tag/v1.15.0)

[1.12.1]
* Remove broken reset password link

[1.13.0]
* Use base image v3

[1.14.0]
* Update Rainloop to 1.16.0
* [changelog](https://www.rainloop.net/changelog/)
* Fixed parsing problems of IMAP commands (folder names)
* Fixed closure warnings (#2087)
* Update dependencies
* Synchronize localizations from Transifex
* Fixes and code improvements

[1.14.1]
* Update base image to v3
* Better apache configs

[1.14.2]
* Fixes XSS code flaw - https://blog.sonarsource.com/rainloop-emails-at-risk-due-to-code-flaw

[1.15.0]
* Update Rainloop to 1.17.0
* Relicensed to MIT

