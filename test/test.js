/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

// EMAIL1 and EMAIL2 are two mailboxes in the cloudron of different domains
if (!process.env.PASSWORD || !process.env.EMAIL1 || !process.env.EMAIL2) {
    console.log('EMAIL1, EMAIL2 and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EMAIL1 = process.env.EMAIL1, EMAIL2 = process.env.EMAIL2;
    const FQDN = 'test.' + EMAIL1.split('@')[1], FQDN2 = 'test2.' + EMAIL1.split('@')[1];
    const MAIL_0_TO = EMAIL2;
    const subject = 'Test subject ' + Math.random();
    const MAIL_0_CONTENT = 'Test content 0';
    const CONTACT_0_NAME = 'Herbert';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    async function login(email) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.id('RainLoopEmail')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.id('RainLoopEmail'))), TEST_TIMEOUT);
        await browser.sleep(3000);
        await browser.findElement(By.id('RainLoopEmail')).sendKeys(email);
        await browser.findElement(By.id('RainLoopPassword')).sendKeys(process.env.PASSWORD);
        await browser.findElement(By.className('loginForm')).submit();
        await browser.wait(until.elementLocated(By.className('buttonCompose')), TEST_TIMEOUT);
    }

    async function enableAdmin() {
        execSync(`cloudron exec --app ${app.fqdn} -- sed -e 's/allow_admin_panel = .*/allow_admin_panel = On/g' -i /app/data/_data_/_default_/configs/application.ini`, EXEC_ARGS);
    }

    async function adminLogin() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/?admin');
        await browser.wait(until.elementLocated(By.id('RainLoopAdminLogin')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.id('RainLoopAdminLogin'))), TEST_TIMEOUT);
        await browser.findElement(By.id('RainLoopAdminLogin')).sendKeys('admin');
        await browser.findElement(By.id('RainLoopAdminPassword')).sendKeys('12345');
        await browser.findElement(By.className('loginForm')).submit();
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Admin Panel")]')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.id('top-system-dropdown-id')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.id('top-system-dropdown-id'))), TEST_TIMEOUT);
        await browser.findElement(By.id('top-system-dropdown-id')).click();
        await browser.wait(until.elementLocated(By.xpath('//*[text()="Logout"]')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//*[text()="Logout"]'))), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//*[text()="Logout"]')).click();
        await browser.wait(until.elementLocated(By.id('RainLoopEmail')), TEST_TIMEOUT);
    }

    async function sendMail(subject) {
        await browser.get('https://' + app.fqdn);

        const editor = '//*[@id="cke_1_contents"]/textarea';

        await browser.wait(until.elementLocated(By.className('buttonCompose')), TEST_TIMEOUT);
        await browser.findElement(By.className('buttonCompose')).click();
        await browser.wait(until.elementLocated(By.xpath(editor)), 120000); // sometimes this needs a manual click
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath(editor))), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//*[@id="rl-popups"]/div/div/div/div[2]/div/div[1]/div/div[2]/div[2]/ul/li[1]/input')).sendKeys(MAIL_0_TO);
        await browser.findElement(By.xpath('//*[@id="rl-popups"]/div/div/div/div[2]/div/div[1]/div/div[6]/div[2]/input')).sendKeys(subject);
        await browser.findElement(By.xpath(editor)).sendKeys(MAIL_0_CONTENT);
        await browser.findElement(By.xpath('//*[@id="rl-popups"]/div/div/div/div[1]/a[1]')).click();
        await browser.sleep(5000);
    }

    async function getMail(subject)  {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//*[text()="' + subject + '"]')), TEST_TIMEOUT);
    }

    async function addContact() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.className('buttonContacts')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.className('buttonContacts'))), TEST_TIMEOUT);
        await browser.findElement(By.className('buttonContacts')).click();
        await browser.wait(until.elementLocated(By.className('button-create-contact')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.className('button-create-contact'))), TEST_TIMEOUT);
        await browser.findElement(By.className('button-create-contact')).click();
        await browser.wait(until.elementLocated(By.xpath('//*[@id="rl-popups"]/div/div/div/div[2]/div[5]/div[1]/div/div[2]/div/div[1]/div/div[1]/div[1]/input')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//*[@id="rl-popups"]/div/div/div/div[2]/div[5]/div[1]/div/div[2]/div/div[1]/div/div[1]/div[1]/input'))), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//*[@id="rl-popups"]/div/div/div/div[2]/div[5]/div[1]/div/div[2]/div/div[1]/div/div[1]/div[1]/input')).sendKeys(CONTACT_0_NAME);
        await browser.findElement(By.className('button-save-contact ')).click();
        await browser.sleep(4000); // give some time to do the request
    }

    async function getContact() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.className('buttonContacts')), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(By.className('buttonContacts'))), TEST_TIMEOUT);
        await browser.findElement(By.className('buttonContacts')).click();
        await browser.wait(until.elementLocated(By.xpath('//*[text()="' + CONTACT_0_NAME + '"]')), TEST_TIMEOUT);
    }

    async function viewPGP() {
        await browser.get('https://' + app.fqdn + '/#/settings/openpgp');
        await browser.sleep(3000);
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Import OpenPGP Key")]')), TEST_TIMEOUT);
    }

    async function checkDataAccess() {
        const response = await superagent.get('https://' + app.fqdn + '/data/VERSION').ok(() => true);
        if (response.status === 403) return;

        console.dir(response);

        throw new Error('Was able to access version');
    }

    async function checkFilters() {
        await browser.get('https://' + app.fqdn + '/#/settings/filters');
        await browser.sleep(3000); // why?
        // otherwise it says "Can't connect to server"
        await browser.wait(until.elementIsVisible(browser.findElement(By.xpath('//span[contains(text(), "Add a Filter")]'))), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.fqdn === FQDN || a.fqdn === FQDN2; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + FQDN, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, EMAIL1));
    it('can send mail', sendMail.bind(null, subject));
    it('check filters', checkFilters);
    it('view PGP', viewPGP);
    it('add contact', addContact);
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, EMAIL2));
    it('get mail', getMail.bind(null, subject));
    it('can logout', logout);

    it('can enable admin', enableAdmin);
    it('can access admin', adminLogin);

    it('cannot access data', checkDataAccess);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, EXEC_ARGS);
    });

    it('can login', login.bind(null, EMAIL1));
    it('view PGP', viewPGP);
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, EMAIL2));
    it('get mail', getMail.bind(null, subject));
    it('can logout', logout);

    it('cannot access data', checkDataAccess);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + FQDN2 + ' --app ' + app.id, EXEC_ARGS);
        getAppInfo();
    });

    it('can login', login.bind(null, EMAIL1));
    it('view PGP', viewPGP);
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, EMAIL2));
    it('get mail', getMail.bind(null, subject));
    it('check filters', checkFilters);
    it('can logout', logout);

    it('can access admin', adminLogin);

    it('cannot access data', checkDataAccess);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id net.rainloop.cloudronapp --location ' + FQDN, EXEC_ARGS);
        getAppInfo();
    });
    it('can login', login.bind(null, EMAIL1));
    it('can send mail', sendMail.bind(null, subject));
    it('add contact', addContact);
    it('can enable admin', enableAdmin);
    it('can update', function () {
        execSync('cloudron update --app ' + FQDN, EXEC_ARGS);
    });
    it('get contact', getContact);
    it('can logout', logout);

    it('can login', login.bind(null, EMAIL2));
    it('get mail', getMail.bind(null, subject));
    it('check filters', checkFilters);
    it('can logout', logout);

    it('can access admin', adminLogin);

    it('cannot access data', checkDataAccess);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});

